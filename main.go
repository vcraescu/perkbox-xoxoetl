package main

import (
	"context"
	"gitlab.com/vcraescu/perkbox-xoxoetl/internal/cron"
	"gitlab.com/vcraescu/perkbox-xoxoetl/internal/ingest"
	"gitlab.com/vcraescu/perkbox-xoxoetl/pkg/log"
	"gitlab.com/vcraescu/perkbox-xoxoetl/pkg/xoxo"
)

func main() {
	ctx := context.Background()

	client := xoxo.NewClient()
	logger := log.NewLogger()
	offerRepository := ingest.NewOfferRepository()
	handler := cron.NewService(client, offerRepository, logger)

	if err := handler.Run(ctx); err != nil {
		panic(err)
	}
}
