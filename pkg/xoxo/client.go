package xoxo

import (
	"context"
	"errors"
)

var ErrPageNotFound = errors.New("page not found")

type Client struct {
}

type Deal struct {
	ID          string `json:"id,omitempty"`
	Name        string `json:"name,omitempty"`
	Description string `json:"description,omitempty"`
}

func NewClient() *Client {
	return &Client{}
}

func (c *Client) Fetch(ctx context.Context, page int) ([]*Deal, error) {
	return nil, nil
}
