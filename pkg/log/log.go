package log

import (
	"log"
	"os"
)

type Logger struct {
	info  *log.Logger
	error *log.Logger
}

func NewLogger() *Logger {
	return &Logger{
		info:  log.New(os.Stdout, "INFO ", log.LstdFlags),
		error: log.New(os.Stderr, "ERROR ", log.LstdFlags),
	}
}

func (l *Logger) Info(msg string) {
	l.info.Println(msg)
}

func (l *Logger) Error(msg string) {
	l.error.Println(msg)
}
