package offer

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"gitlab.com/vcraescu/perkbox-xoxoetl/internal/ingest"
	"time"
)

const TTL = time.Hour * 24 * 90

type PendingDealRepository interface {
	Save(ctx context.Context, deal *ingest.PendingDeal) error
}

type EventListener struct {
	repository PendingDealRepository
	Clock      func() time.Time
}

func NewEventListener(repository PendingDealRepository) *EventListener {
	return &EventListener{
		repository: repository,
		Clock:      time.Now,
	}
}

func (l *EventListener) Run(ctx context.Context, events <-chan ingest.OfferEvent) error {
	for event := range events {
		if err := l.handleEvent(ctx, event); err != nil {
			return fmt.Errorf("handle event: %w", err)
		}
	}

	return nil
}

func (l *EventListener) handleEvent(ctx context.Context, event ingest.OfferEvent) error {
	switch event.Type {
	case ingest.OfferEventInsertType:
		return l.handleInsertEvent(ctx, event)
	case ingest.OfferEventModifyType:
		return l.handleModifyEvent(ctx, event)
	}

	return fmt.Errorf("unknown event type: %s", event.Type)
}

func (l *EventListener) handleInsertEvent(ctx context.Context, event ingest.OfferEvent) error {
	offer := event.New

	data, err := insertStatusIntoData([]byte(offer.Data), "new")
	if err != nil {
		return fmt.Errorf("insert status into data: %w", err)
	}

	pendingDeal := &ingest.PendingDeal{
		ExternalID: offer.ID,
		Data:       string(data),
		Provider:   offer.Provider,
		Status:     ingest.ReadyForProcessingStatus,
		TTL:        l.Clock().Add(TTL).Unix(),
	}

	return l.repository.Save(ctx, pendingDeal)
}

func (l *EventListener) handleModifyEvent(ctx context.Context, event ingest.OfferEvent) error {
	if event.New.Checksum == event.Old.Checksum {
		return nil
	}

	offer := event.New

	status := "updated"
	if offer.DeletedAt > 0 {
		status = "deleted"
	}

	data, err := insertStatusIntoData([]byte(offer.Data), status)
	if err != nil {
		return fmt.Errorf("insert status into data: %w", err)
	}

	pendingDeal := &ingest.PendingDeal{
		ExternalID: offer.ID,
		Data:       string(data),
		Provider:   offer.Provider,
		Status:     ingest.ReadyForProcessingStatus,
		TTL:        l.Clock().Add(TTL).Unix(),
	}

	return l.repository.Save(ctx, pendingDeal)
}

func insertStatusIntoData(data []byte, status string) ([]byte, error) {
	var m map[string]interface{}

	if err := json.NewDecoder(bytes.NewReader(data)).Decode(&m); err != nil {
		return nil, err
	}

	m["_status"] = status

	w := &bytes.Buffer{}
	if err := json.NewEncoder(w).Encode(m); err != nil {
		return nil, err
	}

	return w.Bytes(), nil
}
