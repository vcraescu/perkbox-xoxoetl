package cron_test

import (
	"context"
	"fmt"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
	"gitlab.com/vcraescu/perkbox-xoxoetl/internal/cron"
	"gitlab.com/vcraescu/perkbox-xoxoetl/internal/ingest"
	"gitlab.com/vcraescu/perkbox-xoxoetl/internal/offer"
	"gitlab.com/vcraescu/perkbox-xoxoetl/pkg/log"
	"gitlab.com/vcraescu/perkbox-xoxoetl/pkg/xoxo"
	"testing"
	"time"
)

var _ cron.XoxoClient = (*XoxoClient)(nil)

type XoxoClient struct {
	mock.Mock
}

func (c *XoxoClient) Fetch(ctx context.Context, page int) ([]*xoxo.Deal, error) {
	args := c.Called(ctx, page)
	out, _ := args.Get(0).([]*xoxo.Deal)

	return out, args.Error(1)
}

func TestService_RunInitOffers(t *testing.T) {
	t.Parallel()

	client := &XoxoClient{}
	client.
		On("Fetch", mock.Anything, 1).
		Return(
			[]*xoxo.Deal{
				{
					ID: "1",
				},
			}, nil).
		Once()

	client.
		On("Fetch", mock.Anything, 2).
		Return(
			[]*xoxo.Deal{
				{
					ID: "2",
				},
			}, nil).
		Once()

	client.
		On("Fetch", mock.Anything, 3).
		Return(nil, xoxo.ErrPageNotFound).
		Once()

	logger := log.NewLogger()
	now := time.Date(2022, 1, 1, 10, 30, 0, 0, time.UTC)

	offerRepository := ingest.NewOfferRepository()
	pendingDealRepository := ingest.NewPendingDealRepository()
	pendingDealRepository.IDGenerator = newIncrementalIDGenerator(t)

	offerListener := offer.NewEventListener(pendingDealRepository)
	offerListener.Clock = func() time.Time {
		return now
	}

	ctx := context.Background()

	go func() {
		err := offerListener.Run(ctx, offerRepository.EventCh)
		require.NoError(t, err)
	}()

	cronService := cron.NewService(client, offerRepository, logger)
	cronService.Clock = func() time.Time {
		return now
	}

	err := cronService.Run(ctx)
	require.NoError(t, err)

	{
		actual, err := offerRepository.FindAll(ctx)
		require.NoError(t, err)
		require.Len(t, actual, 2)

		want := map[string]ingest.Offer{
			"1": {
				ID:        "1",
				Data:      "{\"id\":\"1\"}\n",
				Checksum:  cron.Checksum([]byte("{\"id\":\"1\"}\n")),
				VisitedAt: now.Unix(),
				Provider:  ingest.XoxoProvider,
			},
			"2": {
				ID:        "2",
				Data:      "{\"id\":\"2\"}\n",
				Checksum:  cron.Checksum([]byte("{\"id\":\"2\"}\n")),
				VisitedAt: now.Unix(),
				Provider:  ingest.XoxoProvider,
			},
		}

		for id, wantOffer := range want {
			gotOffer, err := offerRepository.Find(ctx, id)
			require.NoError(t, err)

			require.Equal(t, wantOffer, gotOffer)
		}
	}

	{
		time.Sleep(time.Second)
		actual, err := pendingDealRepository.FindAll(ctx)
		require.NoError(t, err)
		require.Len(t, actual, 2)

		want := map[string]ingest.PendingDeal{
			"1": {
				ID:         "1",
				ExternalID: "1",
				Data:       "{\"_status\":\"new\",\"id\":\"1\"}\n",
				Provider:   ingest.XoxoProvider,
				Status:     ingest.ReadyForProcessingStatus,
				TTL:        now.Add(offer.TTL).Unix(),
			},
			"2": {
				ID:         "2",
				ExternalID: "2",
				Data:       "{\"_status\":\"new\",\"id\":\"2\"}\n",
				Provider:   ingest.XoxoProvider,
				Status:     ingest.ReadyForProcessingStatus,
				TTL:        now.Add(offer.TTL).Unix(),
			},
		}

		for id, wantDeal := range want {
			gotDeal, err := pendingDealRepository.Find(ctx, id)

			require.NoError(t, err)
			require.Equal(t, wantDeal, gotDeal)
		}
	}
}

func TestHandler_HandleWithUpdatesInsertsAndDeletes(t *testing.T) {
	t.Parallel()

	ctx := context.Background()
	createdAt := time.Date(2021, 1, 1, 10, 30, 0, 0, time.UTC)
	now := time.Date(2022, 1, 1, 10, 30, 0, 0, time.UTC)

	offerRepository := ingest.NewOfferRepository()
	pendingDealRepository := ingest.NewPendingDealRepository()
	pendingDealRepository.IDGenerator = newIncrementalIDGenerator(t)

	offerListener := offer.NewEventListener(pendingDealRepository)
	offerListener.Clock = func() time.Time {
		return now
	}

	go func() {
		err := offerListener.Run(ctx, offerRepository.EventCh)
		require.NoError(t, err)
	}()

	err := offerRepository.Save(
		ctx,
		&ingest.Offer{
			ID:        "1",
			Data:      "{\"id\":\"1\"}\n",
			Checksum:  cron.Checksum([]byte("{\"id\":\"1\"}\n")),
			VisitedAt: createdAt.Unix(),
			Provider:  ingest.XoxoProvider,
		},
		&ingest.Offer{
			ID:        "2",
			Data:      "{\"id\":\"2\"}\n",
			Checksum:  cron.Checksum([]byte("{\"id\":\"2\"}\n")),
			VisitedAt: createdAt.Unix(),
			Provider:  ingest.XoxoProvider,
		},
		&ingest.Offer{
			ID:        "3",
			Data:      "{\"id\":\"3\"}\n",
			Checksum:  cron.Checksum([]byte("{\"id\":\"3\"}\n")),
			VisitedAt: createdAt.Unix(),
			Provider:  ingest.XoxoProvider,
		},
	)
	require.NoError(t, err)

	client := &XoxoClient{}
	client.
		On("Fetch", mock.Anything, 1).
		Return(
			[]*xoxo.Deal{
				{
					ID: "1",
				},
				{
					ID:   "2",
					Name: "Updated",
				},
				{
					ID:   "4",
					Name: "Insert",
				},
			}, nil).
		Once()

	client.
		On("Fetch", mock.Anything, 2).
		Return(nil, xoxo.ErrPageNotFound).
		Once()

	logger := log.NewLogger()

	cronService := cron.NewService(client, offerRepository, logger)
	cronService.Clock = func() time.Time {
		return now
	}

	err = cronService.Run(ctx)
	require.NoError(t, err)

	{
		actual, err := offerRepository.FindAll(ctx)
		require.NoError(t, err)
		require.Len(t, actual, 4)

		want := map[string]ingest.Offer{
			"1": {
				ID:        "1",
				Data:      "{\"id\":\"1\"}\n",
				Checksum:  cron.Checksum([]byte("{\"id\":\"1\"}\n")),
				VisitedAt: now.Unix(),
				Provider:  ingest.XoxoProvider,
			},
			"2": {
				ID:        "2",
				Data:      "{\"id\":\"2\",\"name\":\"Updated\"}\n",
				Checksum:  cron.Checksum([]byte("{\"id\":\"2\",\"name\":\"Updated\"}\n")),
				VisitedAt: now.Unix(),
				Provider:  ingest.XoxoProvider,
			},
			"3": {
				ID:        "3",
				Data:      "{\"id\":\"3\"}\n",
				VisitedAt: now.Unix(),
				DeletedAt: now.Unix(),
				Provider:  ingest.XoxoProvider,
				TTL:       now.Add(cron.DeletedTTL).Unix(),
			},
			"4": {
				ID:        "4",
				Data:      "{\"id\":\"4\",\"name\":\"Insert\"}\n",
				Checksum:  cron.Checksum([]byte("{\"id\":\"4\",\"name\":\"Insert\"}\n")),
				VisitedAt: now.Unix(),
				Provider:  ingest.XoxoProvider,
			},
		}

		for id, wantDeal := range want {
			gotDeal, err := offerRepository.Find(ctx, id)
			require.NoError(t, err)
			require.Equal(t, wantDeal, gotDeal)
		}
	}

	{
		time.Sleep(time.Second)
		actual, err := pendingDealRepository.FindAll(ctx)
		require.NoError(t, err)
		require.Len(t, actual, 6)

		want := map[string]ingest.PendingDeal{
			"1": {
				ID:         "1",
				ExternalID: "1",
				Data:       "{\"_status\":\"new\",\"id\":\"1\"}\n",
				Provider:   ingest.XoxoProvider,
				Status:     ingest.ReadyForProcessingStatus,
				TTL:        now.Add(offer.TTL).Unix(),
			},
			"2": {
				ID:         "2",
				ExternalID: "2",
				Data:       "{\"_status\":\"new\",\"id\":\"2\"}\n",
				Provider:   ingest.XoxoProvider,
				Status:     ingest.ReadyForProcessingStatus,
				TTL:        now.Add(offer.TTL).Unix(),
			},
			"3": {
				ID:         "3",
				ExternalID: "3",
				Data:       "{\"_status\":\"new\",\"id\":\"3\"}\n",
				Provider:   ingest.XoxoProvider,
				Status:     ingest.ReadyForProcessingStatus,
				TTL:        now.Add(offer.TTL).Unix(),
			},
			"4": {
				ID:         "4",
				ExternalID: "2",
				Data:       "{\"_status\":\"updated\",\"id\":\"2\",\"name\":\"Updated\"}\n",
				Provider:   ingest.XoxoProvider,
				Status:     ingest.ReadyForProcessingStatus,
				TTL:        now.Add(offer.TTL).Unix(),
			},
			"5": {
				ID:         "5",
				ExternalID: "4",
				Data:       "{\"_status\":\"new\",\"id\":\"4\",\"name\":\"Insert\"}\n",
				Provider:   ingest.XoxoProvider,
				Status:     ingest.ReadyForProcessingStatus,
				TTL:        now.Add(offer.TTL).Unix(),
			},
			"6": {
				ID:         "6",
				ExternalID: "3",
				Data:       "{\"_status\":\"deleted\",\"id\":\"3\"}\n",
				Provider:   ingest.XoxoProvider,
				Status:     ingest.ReadyForProcessingStatus,
				TTL:        now.Add(offer.TTL).Unix(),
			},
		}

		for id, wantDeal := range want {
			gotDeal, err := pendingDealRepository.Find(ctx, id)
			require.NoError(t, err)
			require.Equal(t, wantDeal, gotDeal)
		}
	}
}

func newIncrementalIDGenerator(t *testing.T) func() string {
	var id int

	return func() string {
		t.Helper()

		id++

		return fmt.Sprint(id)
	}
}
