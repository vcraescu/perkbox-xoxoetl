package cron

import (
	"bytes"
	"context"
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/cespare/xxhash/v2"
	"gitlab.com/vcraescu/perkbox-xoxoetl/internal/ingest"
	"gitlab.com/vcraescu/perkbox-xoxoetl/pkg/xoxo"
	"golang.org/x/sync/errgroup"
	"time"
)

const DeletedTTL = time.Hour * 24 * 90

type XoxoClient interface {
	Fetch(ctx context.Context, page int) ([]*xoxo.Deal, error)
}

type Logger interface {
	Error(msg string)
	Info(msg string)
}

type OfferRepository interface {
	Save(ctx context.Context, deal ...*ingest.Offer) error
	FindUnvisisted(ctx context.Context, provider string, ts int64) ([]ingest.Offer, error)
}

type Service struct {
	repository OfferRepository
	xoxo       XoxoClient
	logger     Logger
	Clock      func() time.Time
}

func NewService(xoxo XoxoClient, repository OfferRepository, logger Logger) *Service {
	return &Service{
		xoxo:       xoxo,
		repository: repository,
		logger:     logger,
		Clock:      time.Now,
	}
}

func (s *Service) Run(ctx context.Context) error {
	g, ctx := errgroup.WithContext(ctx)

	ts := s.Clock().Unix()

	savedOffers := make(map[string]struct{})
	xoxoDealsCh := make(chan *xoxo.Deal, 100)

	g.Go(func() error {
		return s.fetchXoxoDeals(ctx, xoxoDealsCh)
	})

	g.Go(func() error {
		for deal := range xoxoDealsCh {
			if err := s.saveXoxoDeal(ctx, deal, ts); err != nil {
				return err
			}

			savedOffers[deal.ID] = struct{}{}
		}

		return nil
	})

	if err := g.Wait(); err != nil {
		return err
	}

	return s.processDeletions(ctx, ts, savedOffers)
}

func (s *Service) saveXoxoDeal(ctx context.Context, deal *xoxo.Deal, ts int64) error {
	data, err := marshalXoxoDeal(deal)
	if err != nil {
		return fmt.Errorf("marshalXoxoDeal: %w", err)
	}

	offer := &ingest.Offer{
		ID:        deal.ID,
		Provider:  ingest.XoxoProvider,
		Checksum:  Checksum(data),
		Data:      string(data),
		VisitedAt: ts,
	}

	if err := s.repository.Save(ctx, offer); err != nil {
		return fmt.Errorf("save pending deal: %w", err)
	}

	return nil
}

func (s *Service) fetchXoxoDeals(ctx context.Context, out chan<- *xoxo.Deal) error {
	defer close(out)

	for page := 1; page < 1000; page++ {
		deals, err := s.xoxo.Fetch(ctx, page)
		if err != nil {
			if errors.Is(err, xoxo.ErrPageNotFound) {
				return nil
			}

			return err
		}

		if len(deals) == 0 {
			break
		}

		for _, deal := range deals {
			out <- deal
		}
	}

	return nil
}

func (s *Service) processDeletions(ctx context.Context, ts int64, savedOffers map[string]struct{}) error {
	offers, err := s.repository.FindUnvisisted(ctx, ingest.XoxoProvider, ts)
	if err != nil {
		return err
	}

	for _, offer := range offers {
		if _, ok := savedOffers[offer.ID]; ok {
			s.logger.Info(
				fmt.Sprintf("offer already been updated but was found wrong to be out of date: %s", offer.ID),
			)

			continue
		}

		if offer.DeletedAt > 0 {
			s.logger.Info(fmt.Sprintf("offer is already deleted: %s", offer.ID))

			continue
		}

		offer.VisitedAt = ts
		offer.DeletedAt = ts
		offer.Checksum = ""
		offer.TTL = s.Clock().Add(DeletedTTL).Unix()

		if err := s.repository.Save(ctx, &offer); err != nil {
			return err
		}
	}

	return nil
}

func marshalXoxoDeal(deal *xoxo.Deal) ([]byte, error) {
	w := &bytes.Buffer{}

	if err := json.NewEncoder(w).Encode(deal); err != nil {
		return nil, err
	}

	return w.Bytes(), nil
}

func Checksum(b []byte) string {
	h := xxhash.Sum64(b)

	return base64.StdEncoding.EncodeToString([]byte(fmt.Sprint(h)))
}
