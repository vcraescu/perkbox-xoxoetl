package ingest

import (
	"context"
	"errors"
	"github.com/google/uuid"
	"sync"
)

const (
	ReadyForProcessingStatus = "READY_FOR_PROCESSING"
	ProcessedStatus          = "PROCESSED"

	XoxoProvider = "XOXO"
)

var ErrNotFound = errors.New("not found")

type Status string

type PendingDeal struct {
	ID         string
	ExternalID string
	Data       string
	Provider   string
	Status     Status
	TTL        int64
}

type PendingDealRepository struct {
	data        map[string]PendingDeal
	mu          sync.RWMutex
	IDGenerator func() string
}

func NewPendingDealRepository() *PendingDealRepository {
	return &PendingDealRepository{
		data: make(map[string]PendingDeal),
		IDGenerator: func() string {
			return uuid.New().String()
		},
	}
}

func (r *PendingDealRepository) Save(_ context.Context, deal *PendingDeal) error {
	r.mu.Lock()
	defer r.mu.Unlock()

	if deal.ID == "" {
		deal.ID = r.IDGenerator()
	}

	r.data[deal.ID] = *deal

	return nil
}

func (r *PendingDealRepository) Find(_ context.Context, id string) (PendingDeal, error) {
	r.mu.RLock()
	defer r.mu.RUnlock()

	deal, ok := r.data[id]
	if !ok {
		return PendingDeal{}, ErrNotFound
	}

	return deal, nil
}

func (r *PendingDealRepository) FindAll(_ context.Context) ([]PendingDeal, error) {
	r.mu.RLock()
	defer r.mu.RUnlock()

	out := make([]PendingDeal, len(r.data))

	var i int

	for _, deal := range r.data {
		out[i] = deal
		i++
	}

	return out, nil
}
