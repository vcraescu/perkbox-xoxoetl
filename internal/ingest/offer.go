package ingest

import (
	"context"
	"sync"
)

type Offer struct {
	Provider  string
	ID        string
	Checksum  string
	VisitedAt int64
	DeletedAt int64
	Data      string
	TTL       int64
}

const (
	OfferEventInsertType = "insert"
	OfferEventModifyType = "modify"
)

type OfferEvent struct {
	Type string
	New  Offer
	Old  Offer
}

type OfferRepository struct {
	data    map[string]Offer
	mu      sync.RWMutex
	EventCh chan OfferEvent
}

func NewOfferRepository() *OfferRepository {
	return &OfferRepository{
		data:    make(map[string]Offer),
		EventCh: make(chan OfferEvent, 100),
	}
}

func (r *OfferRepository) Save(_ context.Context, offers ...*Offer) error {
	r.mu.Lock()
	defer r.mu.Unlock()

	for _, offer := range offers {
		event := OfferEvent{
			Type: OfferEventInsertType,
			New:  *offer,
		}

		if old, ok := r.data[offer.ID]; ok {
			event.Old = old
			event.Type = OfferEventModifyType
		}

		r.data[offer.ID] = *offer
		r.EventCh <- event
	}

	return nil
}

func (r *OfferRepository) Find(_ context.Context, id string) (Offer, error) {
	r.mu.RLock()
	defer r.mu.RUnlock()

	offer, ok := r.data[id]
	if !ok {
		return Offer{}, ErrNotFound
	}

	return offer, nil
}

func (r *OfferRepository) FindAll(_ context.Context) ([]Offer, error) {
	r.mu.RLock()
	defer r.mu.RUnlock()

	out := make([]Offer, len(r.data))

	var i int

	for _, offer := range r.data {
		out[i] = offer
		i++
	}

	return out, nil
}

func (r *OfferRepository) FindUnvisisted(_ context.Context, provider string, ts int64) ([]Offer, error) {
	r.mu.RLock()
	defer r.mu.RUnlock()

	out := make([]Offer, 0)

	for _, offer := range r.data {
		if offer.Provider == provider && offer.VisitedAt < ts {
			out = append(out, offer)
		}
	}

	return out, nil
}
